﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BranchManager.Models
{
    public class BranchContext : DbContext
    {
        public BranchContext(DbContextOptions<BranchContext> options) : base(options){}

        public DbSet<Branch> branches { get; set; }
    }
}
