﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BranchManager.Models;

namespace BranchManager.Controllers
{
    [Produces("application/json")]
    [Route("api/Branches")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly BranchContext _context;

        public BranchesController(BranchContext context)
        {
            _context = context;
        }

        //Get - Lấy toàn bộ dữ liệu branches
        // GET: api/Branches
        /// <summary>
        /// Lấy toàn bộ dữ liệu branches
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BranchDTO>>> Getbranches()
        {
            //return await _context.branches.ToListAsync();
            return await _context.branches.Select(b => BranchToDTO(b)).ToListAsync();
        }

        //Details - Lấy thông tin chi tiết branch
        // GET: api/Branches/5
        /// <summary>
        /// Lấy thông tin chi tiết branch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<BranchDTO>> GetBranch(int id)
        {
            var branch = await _context.branches.FindAsync(id);

            if (branch == null)
            {
                return NotFound();
            }

            return BranchToDTO(branch);
        }

        // Edit - Sửa branch
        // PUT: api/Branches/5
        /// <summary>
        /// Sửa branch
        /// </summary>
        /// <param name="id"></param>
        /// <param name="branchDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBranch(int id, BranchDTO branchDTO)
        {
            if (id != branchDTO.BranchId)
            {
                return BadRequest();
            }

            var branch = await _context.branches.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            branch.Name = branchDTO.Name;
            branch.Address = branchDTO.Address;
            branch.City = branchDTO.City;
            branch.State = branchDTO.State;
            branch.ZipCode = branchDTO.ZipCode;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!BranchExists(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        //Add - Thêm branch
        // POST: api/Branches
        /// <summary>
        /// Thêm branch
        /// </summary>
        /// <param name="branchDTO"></param>
        /// <returns></returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BranchDTO>> PostBranch(BranchDTO branchDTO)
        {
            Branch branch = new Branch
            {
                Name = branchDTO.Name,
                Address = branchDTO.Address,
                City = branchDTO.City,
                State = branchDTO.State,
                ZipCode = branchDTO.ZipCode
            };

            _context.branches.Add(branch);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetBranch", new { id = branch.BranchId }, branch);
            return CreatedAtAction(nameof(GetBranch), new { id = branch.BranchId }, branch);
        }

        //Remove - Xóa branch
        // DELETE: api/Branches/5
        /// <summary>
        /// Xóa branch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranch(int id)
        {
            var branch = await _context.branches.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            _context.branches.Remove(branch);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BranchExists(int id)
        {
            return _context.branches.Any(e => e.BranchId == id);
        }

        private static BranchDTO BranchToDTO(Branch branch) =>
            new BranchDTO
            {
                BranchId = branch.BranchId,
                Name = branch.Name,
                Address = branch.Address,
                City = branch.City,
                State = branch.State,
                ZipCode = branch.ZipCode
            };
    }
}
